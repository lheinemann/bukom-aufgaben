package Aufgabe1;

import java.io.*;
import java.net.Socket;
import java.util.List;


/**
 * Handles the operations on every client
 */
public class ClientHandler extends Thread {
    private DataOutputStream output;
    private DataInputStream input;
    private Socket clientSocket;
    private FolderManager folderManager;


    /**
     * Constructor saves parameter in local variables
     * @param clientSocket Socket of the client
     * @param folderManager foldermanager to handle the operations with the directory
     */
    public ClientHandler(Socket clientSocket, FolderManager folderManager) throws IOException {
        this.output = new DataOutputStream(clientSocket.getOutputStream());
        this.input = new DataInputStream(clientSocket.getInputStream());
        this.clientSocket = clientSocket;
        this.folderManager = folderManager;
    }


    /**
     * runs the {@link #gettingMessage()} method permanently by checkign for new messages with {@link #gettingMessage()}
     */
    @Override
    public void run() {
        while (true) {
            String message;
            try{
                message = gettingMessage();
            } catch (IOException e) {
                if (!clientSocket.isClosed())
                {
                    return;
                }
                continue;
            }
            handleRequest(message);
        }
    }


    /**
     * handles incoming requests
     * @param request incoming request of the client
     */
    public void handleRequest(String request) {
        System.out.println("Incoming Request:" + request);
        String[] arguments = request.split(" ");

        if (arguments.length == 0) {
            invalidRequest();
        }
        switch (arguments[0]) {
            case "LIST":
                listRequest();
                break;

            case "GET":
                if (arguments.length == 2) {
                    getRequest(arguments[1]);
                } else {
                    invalidRequest();
                }
                break;

            case "QUIT":
                quitRequest();
                break;

            default:
                invalidRequest();
        }
    }


    /**
     * sends message, if request is invalid
     */
    private void invalidRequest()
    {
        System.err.println("Invalid Request received!");
        send("Invalide Request");
    }


    /**
     * process the input LIST
     */
    private void listRequest()
    {
        System.out.println("LIST Request received!");
        List<String> files;
        try {
            files = folderManager.listFilesInFolder();
        }
        catch(IOException ex)
        {
            send("ERROR");
            System.err.println("Error Listing Files!");
            return;
        }

        StringBuilder message = new StringBuilder();
        message.append("Files: \r\n");
        for (String filepath : files) {
            message.append(filepath).append("\r\n");
        }
        send(message.toString());
    }


    /**
     * process the input GET file
     * @param file file to response
     */
    private void getRequest(String file)
    {
        List<String> fileContent;
        try {
            fileContent = folderManager.getFileContent(file);
        }
        catch (IOException ex)
        {
            send("ERROR");
            System.out.println("Error reading File!");
            return;
        }
        StringBuilder message = new StringBuilder();
        for (String line : fileContent) {
            message.append(line).append("\r\n");
        }
        send(message.toString());
    }


    /**
     * waiting for a message in reading it from the Inputstream
     * @return the received Message
     */
    private String gettingMessage () throws IOException {
        System.out.println("Waiting for a message...");
        String message;
        try {
            int messageLength = input.readInt();
            byte[] messageData = new byte[messageLength];
            input.readFully(messageData,0,messageLength);
            message = new String(messageData);
        } catch (IOException e) {
            System.out.println("Something went wrong while waiting for a Message");
            throw e;
        }
        return message;
    }


    /**
     * sends a message to the client
     * @param send message to send
     */
    private void send(String send) {
        try
        {
            byte[] messageData = send.getBytes();
            output.writeInt(messageData.length);
            output.write(messageData);
            System.out.println("You send the command \n" + send + " to the server");
        }
        catch (Exception ex)
        {
            System.out.println ("Something went wrong while sending the message to the server");
        }
    }


    /**
     * process the input QUIT
     */
    private void quitRequest()
    {
        System.out.println("QUIT Request received!");
        try {
            send("Server closed");
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
