package Aufgabe1;
/**
 * <h1>Server-CLient Program</h1>
 * <h3>created by Erik Schulz and Lorenz Heinemann</h3>
 * <h5>Betriebs und Kommunikationssysteme 2019/20</h5>
 * <p>
 * Clients can connect to a server and run three different
 * commands to read from a directory
 *
 * @author  Erik Schulz & Lorenz Heinemann
 * @version 1.0
 * @since   2020-01-15
 */

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Server class
 */
public class Server {

    /** Serversocket to egt access to Clients*/
    private ServerSocket server;

    /** Foldermanager who handels the operations with the directory*/
    private FolderManager folderManager;


    /**
     * Main_methode to create the server and run it on port 6000
     * @param args input
     */
    public static void main (String[] args) {
        Server server = new Server();
        server.server_run(6000);
    }


    /**
     * Method to start run the server by creating {@link #folderManager} and {@link #server}.
     * Creats all needed Streams.
     * Creats new thread for incoming clients and waits for new ones
     * @param port the port to run the server on
     */
    private void server_run(int port) {
        System.out.println("Server is starting...");
        try{
            String folderPath = "C:\\Users\\erikm\\IdeaProjects\\bukom-aufgaben\\files";
            folderManager = new FolderManager(folderPath);
            }
        catch(IOException ex)
            {
            System.out.println("Cannot open Server Directory!");
            return;
            }
        System.out.println("Folder Manager Started!");
        try {
            server = new ServerSocket(port);
        } catch (IOException e) {
            System.out.println("something went wrong while starting the server...");
            e.printStackTrace();
        }
        while (true){
            Socket clientSocket;
            try {
                System.out.println("waiting for client at port " + server.getLocalPort() + "...");
                clientSocket = server.accept();
                System.out.println("Connected to " + clientSocket.getLocalSocketAddress());
            } catch (IOException e) {
                System.out.println("Something went wrong while accepting the Client");
                continue;
            }
            //create a new thread for the client

            Thread thread;
            try {
                thread = new ClientHandler(clientSocket, folderManager);
            } catch (IOException e) {
                System.out.println("Couldn't create client Handler");
                continue;
            }
            System.out.println("Created a new thread for client " + clientSocket.getLocalSocketAddress());
            thread.start();
        }
    }

}
