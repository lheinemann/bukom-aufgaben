package Aufgabe1;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.net.SocketException;
import java.util.Scanner;

/**
 * Client who can connect to the server
 */
public class Client {

    /** Socket of the client*/
    private Socket client;

    /** Inputstream*/
    private DataInputStream input;

    /** Outputstream*/
    private DataOutputStream output;

    /**
     * Main_Method creats Client on localhost:6000
     * @param args input
     */
    public static void main (String[] args) {
        Client client = new Client();
        client.run_client("localhost", 6000);
    }


    /**
     * starting the client by creating all needed Streams and checking frequently for new commands ans answers
     * @param ip IP of the client
     * @param port port of the client
     */
    public void run_client(String ip, int port) {
        System.out.println("Client is starting...");
        try {
            client = new Socket(ip, port);
            input = new DataInputStream(client.getInputStream());
            output = new DataOutputStream(client.getOutputStream());
            if(client.isConnected()) {
                System.out.println("Client connected successfully");
            }
        } catch (IOException e) {
            System.out.println("Client did not start");
            e.printStackTrace();
        }
        System.out.println("Client is started");
        while (true) {
            String request = request();
            send(request);
            waitingForAnswer();
        }
    }


    /**
     * Method to check for input from the user
     * @return input
     */
    private String request() {
        System.out.println("What do you want to do? Please enter your command");
        Scanner input = new Scanner(System.in);
        return input.nextLine();
    }


    /**
     * Method to send a command to the server
     * @param message message to send
     */
    public void send(String message) {
        try
        {
            byte[] messageData = message.getBytes();
            output.writeInt(messageData.length);
            output.write(messageData);
            System.out.println("You send the command " + message + " to the server");
        }
        catch (Exception ex)
        {
            System.out.println("Something went wrong while sending the message to the server");
        }
    }


    /**
     * Method to wait for an answer and read it from Inputstream
     */
    public void waitingForAnswer() {
        System.out.println("Waiting for the answer...");
        try {
            int messageLength = input.readInt();
            System.out.println("Message Length:" + messageLength);
            byte[] messageData = new byte[messageLength];
            input.readFully(messageData,0,messageLength);
            System.out.println("Server response:");
            System.out.println(new String(messageData));
        } catch (IOException e) {
            System.out.println("Something went wrong while reading the message");
        }
        /*
        int buffersize = 1024;
        char[] buffer = new char[buffersize];
        StringBuilder message = new StringBuilder();
        int charsRead = 0;
        try {
            while ((charsRead = input.read(buffer, 0, buffer.length)) > 0) {
                message.append(buffer, 0, charsRead);
            }
        } catch (IOException e) {
            System.out.println("Something went wrong while recieving the answer");
        }
        message.append(buffer, 0, charsRead);
        System.out.println(message.toString());
        */
    }
}
