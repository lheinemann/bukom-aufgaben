package Aufgabe1;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Class to handle the directory
 */
public class FolderManager {

    /** folderdirectory*/
    private File folder;


    /**
     * Constructor creating connection to directory
     * @param path path to directory
     * @throws IOException if directory couldn't be found
     */
    public FolderManager(String path) throws IOException
    {
        String folderPath;
        Path relativePath = Paths.get(path);
        Path absolutePath = relativePath.toAbsolutePath();
        folderPath = absolutePath.toString();
        folder = new File(folderPath);
        if( !folder.exists() || !folder.canRead()){
            throw new IOException("Cannot Read Folder!");
        }
    }


    /**
     * Method to list all files in directory
     * @return List of files in directory
     * @throws IOException if all files couldn't be listed
     */
    public List<String> listFilesInFolder() throws IOException
    {
        try
        {
            Stream<Path> walk = Files.walk(folder.toPath());
            return walk.filter(Files::isRegularFile).map(Path::toString).collect(Collectors.toList());
        }
        catch(IOException ex)
        {
            throw new IOException("Could not List Files", ex);
        }
    }


    /**
     * Method to get the content of one single file
     * @param fileName name of the file
     * @return content of the file
     * @throws IOException if file couldn't be read or found
     */
    public List<String> getFileContent(String fileName) throws IOException
    {
        fileName = fileName + ".txt";
        File file = new File(folder,fileName);
        if(!file.exists() || !file.canRead())
        {
            throw new IOException("Cannot Read File!");
        }
        return Files.readAllLines(file.toPath());
    }
}
