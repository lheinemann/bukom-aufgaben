package Aufgabe2;

import java.sql.Time;
import java.sql.Timestamp;
import java.lang.Math;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Class to save the Weatherdata
 */
public class WeatherData {

    /** minimum of random generated temperature*/
    private static final double temperature_min = -10;

    /** maximum of random generated temperature*/
    private static final double temperature_max = 40;

    /** minimum of random generated humidity*/
    private static final double humidity_min = 0;

    /** maximum of random generated humidity*/
    private static final double humidity_max = 100;


    /** saving temperature*/
    private double temperature;

    /** saving humidity*/
    private double humidity;

    /** timestamp of time where instance was updated*/
    private Timestamp timestamp;

    /** position of User*/
    private String position;


    /**
     * Constructor used for only one parameter {@link #position}.
     * Saving parameter {@link #position} in local variable and sets {@link #timestamp} to current time.
     * @param position position of user
     */
    public WeatherData (String position)
    {
        this(position,0,0,new Timestamp(System.currentTimeMillis()));
    }


    /**
     * Constructor setting local variables
     * @param position Position of user
     * @param temperature measured temperature
     * @param humidity measured humidity
     * @param timestamp timestamp of weatherdata
     */
    public WeatherData (String position, double temperature, double humidity, Timestamp timestamp) {
        this.position = position;
        this.temperature = temperature;
        this.humidity = humidity;
        this.timestamp = timestamp;
    }


    /**
     * Getter
     * @return temperature
     */
    public double getTemperature () {
        return temperature;
    }


    /**
     * Getter
     * @return humidity
     */
    public double getHumidity () {
        return humidity;
    }


    /**
     * Getter
     * @return position
     */
    public String getPosition () {
        return position;
    }


    /**
     * Getter
     * @return timestamp
     */
    public Timestamp getTimestamp () {
        return this.timestamp;
    }


    /**
     * Method which transforms all the information into a singe String separated by comma
     * @return String of weatherdata
     */
    public String ToString()
    {
        String weatherString = "";
        StringBuilder sb = new StringBuilder();
        sb.append(this.position);
        sb.append(",");
        sb.append(this.temperature);
        sb.append(",");
        sb.append(this.humidity);
        sb.append(",");
        sb.append(this.timestamp.toString());
        return sb.toString();
    }


    /**
     * Method which converts a given String into the needed information in weatherdata
     * @param string input data as String
     * @return instance of {@link WeatherData}
     */
    public static WeatherData FromString(String string)
    {
        String[] weatherString = string.split(",");
        double temperature = Double.parseDouble(weatherString[1]);
        double humidity = Double.parseDouble(weatherString[2]);
        String timestampString = weatherString[3];
        Timestamp timestamp = null;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
            Date parsedDate = dateFormat.parse(timestampString);
            timestamp = new java.sql.Timestamp(parsedDate.getTime());
        } catch(Exception e) {
            System.out.println("Something went wrong while parsing from String to Timestamp");
        }

        WeatherData weatherData = new WeatherData(weatherString[0], temperature, humidity, timestamp);
        return weatherData;
    }


    /**
     * Method which generates {@link #temperature} and {@link #humidity} random in declared boarders
     * @param position position of user
     * @return instance of {@link WeatherData} with generated parameters
     */
    public static WeatherData generateWeatherData (String position) {
        WeatherData weatherData = new WeatherData(position, 0, 0, new Timestamp(System.currentTimeMillis()));
        double humidityTEMP = (Math.random() * (humidity_max - humidity_min));
        weatherData.humidity = Math.round(100.0 * humidityTEMP) / 100.0;
        double temperatureTEMP = (Math.random() * (temperature_max - temperature_min)) + ((temperature_min));
        weatherData.temperature = Math.round(100.0 * temperatureTEMP) / 100.0;
        return weatherData;
    }
}
