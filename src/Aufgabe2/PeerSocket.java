package Aufgabe2;

import java.io.IOException;
import java.net.*;
import java.sql.Timestamp;


/**
 * Socket of a Client (Peer)
 */
public class PeerSocket
{
    /** Multicastsocket for sending end receiving IP multicast packets */
    private MulticastSocket socket;

    /** IP of the multicastSocket*/
    private InetAddress multicastAddress;

    /** is running?*/
    private boolean running = false;

    /** Thread for receiving*/
    private Thread receiveThread;

    /** saving the port*/
    private int port;


    /** used {@link PeerClient}*/
    private PeerClient manager;


    /**
     * Constructor saving parameters in local variables
     * @param port saving the port for the PeerSocket
     * @param multicastAddress IP for multicasting
     * @param manager {@link PeerClient} which is using the Socket
     * @throws IOException if socket could not be created
     */
    public PeerSocket(int port, InetAddress multicastAddress, PeerClient manager) throws IOException
    {
        socket = new MulticastSocket(port);
        socket.joinGroup(multicastAddress);
        socket.setLoopbackMode(true);
        this.multicastAddress = multicastAddress;
        this.port = port;
        this.manager = manager;
    }


    /**
     * Methode which starts and run the multicasting
     * @param multicastMessage message which should be broadcasted
     * @throws IOException if the message couldn't be send by the socket
     */
    public void multicast(String multicastMessage) throws IOException {
        byte[] buf = multicastMessage.getBytes();
        DatagramPacket packet = new DatagramPacket(buf, buf.length, multicastAddress, this.port);
        socket.send(packet);
    }


    /**
     * Method which send the message (weatherdata) to an specific IP
     * @param message sending message
     * @param address address to send
     * @throws IOException if socket couldn't send the message
     */
    public void send(String message, InetAddress address) throws IOException {
        byte[] buf = message.getBytes();
        DatagramPacket packet = new DatagramPacket(buf, buf.length, address, this.port);
        socket.send(packet);
    }


    /**
     * Method which starts the run-Thread and the linked run_method
     */
    public void start() {
        receiveThread = new Thread(this::run);
        receiveThread.start();
        running = true;
    }


    /**
     * Method which waits for receiving a datagramPacket and saves the data.
     * In case of Quite the socket will be closed
     */
    public void run() {
        byte[] buf = new byte[256];

        while (running) {
            DatagramPacket packet = new DatagramPacket(buf, buf.length);

            try {
                //Waiting for incoming packages
                socket.receive(packet);
            } catch (IOException e) {
                System.out.println("Couldn't receive a message");
            }
            //saving the included data
            String received = new String(packet.getData(), 0, packet.getLength());
            Timestamp time = new Timestamp(System.currentTimeMillis());
            Message message = new Message(packet.getPort(),packet.getAddress(),received,time);
            manager.addMessage(message);
        }
        //close socket
        try {
            socket.leaveGroup(multicastAddress);
        } catch (IOException e) {
            System.out.println("Couldn't leave the group");
        } finally {
            socket.close();
        }
    }


    /**
     * stops the process be setting while-variable from {@link #run()} to false
     */
    public void stop() {
        running = false;
    }
}
