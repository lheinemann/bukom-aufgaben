package Aufgabe2;

import java.net.InetAddress;
import java.sql.Timestamp;


/**
 * Message class for saving data
 */
public class Message {

    /** Port of message */
    public int port;

    /** IP of message */
    public InetAddress ip;

    /** the main Data */
    public String data;

    /** Time of creating the message */
    public Timestamp timestamp;


    /**
     * Constructor just save the given parameter in local variables
     * @param port Port if message
     * @param ip IP of message
     * @param data main Data
     * @param timestamp Time of creating the message
     */
    public Message(int port, InetAddress ip, String data, Timestamp timestamp) {
        this.port = port;
        this.ip = ip;
        this.data = data;
        this.timestamp = timestamp;
    }
}
