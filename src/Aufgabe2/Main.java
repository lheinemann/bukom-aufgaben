/**
 * <h1>Peer-to-Peer Program</h1>
 * <h3>created by Erik Schulz and Lorenz Heinemann</h3>
 * <h5>Betriebs und Kommunikationssysteme 2019/20</h5>
 * <p>
 * Clients can advertise their random generated weatherdata and recieve
 * from other clients and collect the data
 *
 * @author  Erik Schulz & Lorenz Heinemann
 * @version 1.0
 * @since   2020-01-15
 */

package Aufgabe2;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;


/**
 * Main Class to start a Client (Peer)
 */
public class Main {

    /**
     * Main method to run the program.
     * Creats InetAdress on IP 230.0.0.0.
     * User can enter the position and creats {@link PeerClient} and run it
     * @param args input command
     */
    public static void main (String[] args) {
        InetAddress inetAddress;
        try {
            inetAddress = InetAddress.getByName("230.0.0.0");
        } catch (UnknownHostException e) {
            System.out.println("fail");
            return;
        }
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter your Position");
        String peerPosition = scanner.nextLine();
        //creats instance of PeerClient with given Portrange and advertisement, measurement Period
        PeerClient manager = new PeerClient(peerPosition, 10000,10000,50001,50010,inetAddress);
        System.out.println("Starting advertisement");
        // starts the Client
        manager.Start();
        while(true){
            //
        }
    }
}
