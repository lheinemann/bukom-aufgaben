package Aufgabe2;

import java.io.IOException;
import java.net.InetAddress;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.Semaphore;


/**
 * PeerClient who can send end receive data
 */
public class PeerClient
{
    /** Time between two advertisements */
    private long advertisementPeriod;

    /** IP of muticast*/
    private InetAddress multicastAddress;


    /** Time to wait for an Advertisement*/
    private long measurementPeriod;

    /** Location of the User, entered in {@link Main}*/
    private String location;


    /** Is the program still running?*/
    private boolean running = false;

    /** Thread of advertise*/
    private Thread advertiseThread;

    /** Thread of the process*/
    private Thread processThread;

    /** Thread of the measurement*/
    private Thread measurementThread;


    /** Dictionary which collects the Sockets and their Port*/
    private Dictionary<Integer, PeerSocket> sockets = new Hashtable<>();


    /** Hashmap which collects the received Advertisements with IP and the time*/
    private HashMap<InetAddress, Timestamp> receivedAdvertisements = new HashMap<>();


    /** Queue which collects the received messages*/
    private Queue<Message> messageQueue = new ArrayDeque<>();

    /** controls the running threads */
    private Semaphore queueSemaphore = new Semaphore(1);


    /** saves all the received weatherdata with the Position*/
    private HashMap<String, WeatherData> weatherDataMap = new HashMap<>();


    /**
     * Constructor of {@link PeerClient}
     * Saves parameters in local variables.
     * Generates random weatherdata
     * Creats in portrange on every Port a {@link PeerSocket} and starts it with start_method
     * @param position Position of the Client
     * @param advertisementPeriod Period between two advertisements
     * @param measurementPeriod Period between two measurement
     * @param portMin lower boarder of portrage
     * @param portMax upper boarder of portrange
     * @param multicastAddress IP or multicast
     */
    public PeerClient(String position, long advertisementPeriod, long measurementPeriod,int portMin, int portMax, InetAddress multicastAddress)
    {
        this.location = position;
        this.advertisementPeriod = advertisementPeriod;
        this.measurementPeriod = measurementPeriod;
        this.multicastAddress = multicastAddress;
        WeatherData positionData = WeatherData.generateWeatherData(position);
        weatherDataMap.put(position,positionData);
        for(int i = portMin; i <= portMax; i++) {
            PeerSocket socket;
            try {
                socket = new PeerSocket(i, multicastAddress, this);
            } catch (IOException e) {
                System.out.println("Couldn't create Socket on Port: " + i);
                continue;
            }
            socket.start();
            sockets.put(i, socket);
        }
    }


    /**
     * adds a message to the {@link #queueSemaphore}
     * @param message message to add
     */
    public void addMessage(Message message)
    {
        try {
            queueSemaphore.acquire();
        } catch (InterruptedException e) {
            return;
        }
        messageQueue.add(message);
        queueSemaphore.release();
    }


    /**
     * Start_method to start all Threads
     */
    public void Start()
    {
        advertiseThread = new Thread(this::runAdvertisement);
        processThread = new Thread(this::runProcessing);
        measurementThread = new Thread(this::runMeasurement);
        running = true;
        measurementThread.start();
        processThread.start();
        advertiseThread.start();
    }


    /**
     * Method to start the Measurement and check frequently for new Data
     */
    private void runMeasurement()
    {
        while(running)
        {
            WeatherData newData = WeatherData.generateWeatherData(location);
            updateData(newData);
            try {
                //Sleep time between two measurements
                Thread.sleep(measurementPeriod);
            } catch (InterruptedException e) {
                return;
            }
        }
    }


    /**
     * Method to start the Advertisement and sending Data in portrange
     */
    private void runAdvertisement()
    {
        while(running)
        {
            advertise();
            try {
                //Sleep time between sending Data
                Thread.sleep(advertisementPeriod);
            } catch (InterruptedException e) {
                return;
            }
        }
    }


    /**
     * Method to work off the {@link #messageQueue} and running the threads in order
     */
    private void runProcessing()
    {
        while(running)
        {
            try {
                queueSemaphore.acquire();
            } catch (InterruptedException e) {
                continue;
            }
            if(messageQueue.size() > 0)
            {
                Message message = messageQueue.poll();
                processMessage(message);
            }
            queueSemaphore.release();
        }
    }


    /**
     * Method to check if the received message is an advertisment_package or weatherdata and handle the data
     * If advertisment_package: check if it is a new package, then send own {@link WeatherData} as response as String.
     * If weatherdata: add to Collection of received data
     * @param message received message
     */
    private void processMessage(Message message)
    {
        //if the received data is an advertisement_package
        if(message.data.startsWith("WeatherAdvertisement"))
        {
            //if it is not a new package replace with old package if timeDelta is great enough
            if(receivedAdvertisements.containsKey(message.ip))
            {
                Timestamp lastReceiveTime = receivedAdvertisements.get(message.ip);
                long timeDelta = message.timestamp.getTime() - lastReceiveTime.getTime();
                if(timeDelta > (advertisementPeriod / 2))
                {
                    receivedAdvertisements.replace(message.ip, message.timestamp);
                }
                else{
                    return;
                }
            }
            //if it is a new package add to List
            else {
                receivedAdvertisements.put(message.ip, message.timestamp);
            }

            System.out.println("Processing WeatherAdvertisement");

            //generates String of all weatherdata to send
            String data = weatherDataListToString(weatherDataMap.values());
            String weatherDataString = "WeatherData " + data;
            PeerSocket socket = sockets.get(message.port);
            try {
                socket.send(weatherDataString, message.ip);
            } catch (IOException e) {
                System.out.println("Cannot send reply!");
            }
        }
        //if the received data are weatherdata save in collection
        else if(message.data.startsWith("WeatherData"))
        {
            int index = message.data.indexOf(" ");
            //cut of the identifier prefix
            String dataString = message.data.substring(index + 1);
            Collection<WeatherData> receivedData = stringToWeatherDataList(dataString);
            for(WeatherData data : receivedData)
            {
                updateData(data);
            }
        }
        printWeatherData();
    }


    /**
     * Method to advertise and start multicasting
     */
    private void advertise()
    {
        System.out.println("Advertising");
        String message;
        var keys = sockets.keys();

        while(keys.hasMoreElements())
        {
            int key = keys.nextElement();
            PeerSocket socket = sockets.get(key);
            try {
                socket.multicast("WeatherAdvertisement");
            } catch (IOException e) {
                System.out.println("Couldn't multicast");
            }
        }
    }


    /**
     * creats String with all weatherdata seperated by semicolon
     * @param list all weatherdata collected in a list
     * @return list converted in String
     */
    private String weatherDataListToString (Collection<WeatherData> list) {
        StringBuilder sb = new StringBuilder();
        for(WeatherData data : list)
        {
            //converts one instance of weatherdata to String seperated by space
            sb.append(data.ToString());
            sb.append(";");
        }
        return sb.toString();
    }


    /**
     * generates list of {@link WeatherData} from the given String
     * @param string Information received of weatherdata
     * @return generated list of {@link WeatherData}
     */
    private Collection<WeatherData> stringToWeatherDataList(String string)
    {
        String[] splitString = string.split(";");
        Collection<WeatherData> dataList = new ArrayList<>();
        for(String weatherDataString : splitString)
        {
            WeatherData data = WeatherData.FromString(weatherDataString);
            dataList.add(data);
        }
        return dataList;
    }


    /**
     * Method to update the weatherdata
     * @param weatherData given weatherdata to compare
     */
    private void updateData (WeatherData weatherData) {
        System.out.println("Updating Location: " + weatherData.getPosition());
        //if weatherdata from the position already exists
        if(weatherDataMap.containsKey(weatherData.getPosition())){
            WeatherData presentData = weatherDataMap.get(weatherData.getPosition());
            //if weatherdata is older then current data
            if(presentData.getTimestamp().before(weatherData.getTimestamp()))
            {
                weatherDataMap.replace(weatherData.getPosition(),weatherData);
            }
        }
        //if weatherdata is new
        else{
            System.out.println("neu angelegt!");
            weatherDataMap.put(weatherData.getPosition(),weatherData);
        }
    }

    /**
     * prints all weatherdata from {@link #weatherDataMap}
     */
    private void printWeatherData () {
        for (String entry: weatherDataMap.keySet()) {
            String key = entry.toString();
            WeatherData value = weatherDataMap.get(entry);
            System.out.println("\nThe current Weather for " + value.getPosition().toUpperCase() + " is:");
            System.out.println("Time of recording: " + value.getTimestamp().toString());
            System.out.println("Temperature: " + value.getTemperature());
            System.out.println("Humidity: " + value.getHumidity() + "\n");
        }
    }


    /**
     * returns {@link #weatherDataMap}
     * @return {@link #weatherDataMap}
     */
    public Map getData () {
        return weatherDataMap;
    }
}